<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('/register','UserController@register');
$router->post('/login','UserController@login');
$router->get('/getSpacificUser/{id}','UserController@getSpacificUser');
$router->get('/getAllUsers','UserController@getAllUsers');
$router->put('/update/{id}',['middleware'=>'auth','uses'=>'UserController@update']);
$router->delete('/delete/{id}',['middleware'=>'auth','uses'=>'UserController@delete']);
//$router->delete('/delete/{id}','UserController@delete');
//$router->get('/sendmail','UserController@sendMail');
$router->get('/activation/{api_token}',['as'=>'activation','uses'=>'UserController@activation']);
