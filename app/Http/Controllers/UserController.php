<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;




class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request)
    {
       $user= User::where('email',$request->input('email'))->first();
       if(Hash::check($request->password, $user->password) && $user->activated==1){
        

           return response()->json(['status'=>'success','message'=>'logged in successfuly','user'=>$user]);
       }
      
       elseif($user->activated==0){
           return response()->json(['status'=>'failed','message'=>'check your mail for activation']);
       }
       else{
           return response()->json(['status'=>'failed','message'=>'invalid login']);
       }
    }

    public function register(Request $request)
    {
        $api_token= str_random(10);
        $user= new User();
        $user->name= $request->input('name');
        $user->email=$request->input('email');
        $user->password=app('hash')->make($request->input('password'));
        $user->api_token= $api_token;
        if($user->save())
        {
           
            
            Mail::send('mail',array(

                'email'=>$user->email,
                'api_token'=>$user->api_token),
        function($msg) use($user){ 
                $msg->to($user->email); 
                $msg->from(['ahmedfathimamdouh25996@gmail.com']);
                $msg->subject('Activation Mail');
            }); 
            
                       return response()->json(['status' =>'success','user'=>$user]);
        }
          

         
    }
       
      
        
        
    
   /* 
    public function sendMail()
    {
        
        Mail::raw('Raw string email', function($msg) { 
            $msg->to(['ahmedfathimamdouh25996@yahoo.com']); 
            $msg->from(['ahmedfathimamdouh25996@gmail.com']);
            $msg->subject('Activation Mail');
        });
        }
*/
      
    

    public function activation($api_token)
    {
        $user= User::where('api_token',$api_token)->whereactivated(0)->first();
        if( $user)
        {
            $user->activated= 1;
            
            $user->save();
        }
    }

    public function getSpacificUser($id)
    {
        $user=User::find($id);
        return response()->json(['user'=>$user]);

    }

    public function getAllUsers()
    {
         $users=User::all();
         return response()->json(['status'=>'success','users'=>$users]);
    }

    public function update(Request $request,$id)
    { 
        //you need to add api_token to be authorized to use update function

        $user= User::find($id);
        $user->name=$request->input('name');
        $user->email=$request->input('email');
        $user->password=app('hash')->make($request->input('password'));
        if($user->save()){
            return response()->json(['status' =>'success','user'=>$user]);

        }

    }

    public function delete($id)
    { 
        //you need to add api_token to be authorized to use delete function

        $user=User::find($id);
        if($user->delete()){
            return response()->json(['status' =>'success','message'=>'user deleted successfuly']);
        }


    }
}
